package ch.mobi.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.junit.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SimpleUrlBuilderTest {

    @Test
    public void buildShouldBuildFullUrl() {
        //when
        String url = SimpleUrlBuilder.build(b -> b.path("host")
                                                  .path("path")
                                                  .queryParameter("key1", "val1")
                                                  .queryParameter("key2", "val2"));

        //then
        assertThat(url, is("host/path?key1=val1&key2=val2"));
    }

    @Test
    public void buildShouldBuildUrlWithoutParameter() {
        //when
        String url = SimpleUrlBuilder.build(b -> b.path("host")
                                                  .path("path"));

        //then
        assertThat(url, is("host/path"));
    }

    @Test
    public void buildShouldNotAddNullQueryValues() {
        //when
        String url = SimpleUrlBuilder.build(b -> b.path("path")
                                                  .queryParameter("key1", "val1")
                                                  .queryParameter("key2", (String) null));

        //then
        assertThat(url, is("path?key1=val1"));
    }

    @Test
    public void buildShouldNotAddEmptyQueryValues() {
        //when
        String url = SimpleUrlBuilder.build(b -> b.path("path")
                                                  .queryParameter("key1", "val1")
                                                  .queryParameter("key2", ""));

        //then
        assertThat(url, is("path?key1=val1"));
    }

    @Test
    public void buildShouldBuildWithMultipleQueryValues() {
        //when
        String url = SimpleUrlBuilder.build(b -> b.path("host")
                                                  .queryParameter("key1", asList("val1", "val2")));

        //then
        assertThat(url, is("host?key1=val1&key1=val2"));
    }

    @Test
    public void buildShouldBuildUrlWithoutDoubleSlashes() {
        //when
        String url = SimpleUrlBuilder.build(b -> b.path("http://host/")
                                                  .path("/path/"));

        //then
        assertThat(url, is("http://host/path"));
    }

    @Test
    public void buildShouldBuildUrlEvenIfPathIsNull() {
        //when
        String url = SimpleUrlBuilder.build(b -> b.path("http://host/")
                                                  .path(null));

        //then
        assertThat(url, is("http://host"));
    }

    @Test
    public void buildShouldBuildUrlEvenIfPathIsEmpty() {
        //when
        String url = SimpleUrlBuilder.build(b -> b.path("http://host/")
                                                  .path(""));

        //then
        assertThat(url, is("http://host"));
    }

    @Test
    public void buildShouldBuildUrlEvenIfQueryParameterKeyIsNull() {
        //when
        String url = SimpleUrlBuilder.build(b -> b.path("http://host/")
                                                  .queryParameter(null, ""));

        //then
        assertThat(url, is("http://host"));
    }

    @Test
    public void buildShouldBuildUrlEvenIfQueryParameterIsEmpty() {
        //when
        String url = SimpleUrlBuilder.build(b -> b.path("http://host/")
                                                  .queryParameter("", ""));

        //then
        assertThat(url, is("http://host"));
    }

    @Test
    public void buildShouldBuildUrlEvenIfQueryParametersKeyIsNull() {
        //when
        String url = SimpleUrlBuilder.build(b -> b.path("http://host/")
                                                  .queryParameter(null, singletonList("")));

        //then
        assertThat(url, is("http://host"));
    }

    @Test
    public void buildShouldBuildUrlEvenIfQueryParametersIsEmpty() {
        //when
        String url = SimpleUrlBuilder.build(b -> b.path("http://host/")
                                                  .queryParameter("", singletonList("")));

        //then
        assertThat(url, is("http://host"));
    }

}
