package ch.mobi.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.liima.client.dto.DeploymentState;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class LiimaConfigurationTest {

    public static final String BASE_URL = "http://localhost";

    @Test
    public void getClosestPastReleasePathShouldReturnPathWithType() {
        //given
        LiimaConfiguration configuration = new LiimaConfiguration(BASE_URL);

        //when
        String url = configuration.getClosestPastReleaseUrl("resource", "release", "relation");

        //then
        assertThat(url, startsWith(BASE_URL + "/resources/resource/lte/release?"));
        assertThat(url, containsString("type=relation"));
    }

    @Test
    public void getResourcesUrlShouldReturnPlainPathForMissingParameters() {
        //given
        LiimaConfiguration configuration = new LiimaConfiguration(BASE_URL);

        //when
        String url = configuration.getDeploymentsUrl(null, null, null, null, null);

        //then
        assertThat(url, is(BASE_URL + "/deployments/filter?filters=%5B%5D"));
    }

    @Test
    public void getResourcesUrlShouldReturnPlainPath() {
        //given
        LiimaConfiguration configuration = new LiimaConfiguration(BASE_URL);

        //when
        String url = configuration.getDeploymentsUrl(DeploymentState.success, asList("server_1", "server_2"), asList("app_a", "app_b"), asList("TTT", "PPP"), true);

        //then
        assertThat(url, startsWith(BASE_URL + "/deployments/filter?filters="));
        assertThat(url, containsString("State"));
        assertThat(url, containsString("success"));
        assertThat(url, containsString("Application%20server"));
        assertThat(url, containsString("server_1"));
        assertThat(url, containsString("Application%20server"));
        assertThat(url, containsString("server_2"));
        assertThat(url, containsString("name"));
        assertThat(url, containsString("app_a"));
        assertThat(url, containsString("name"));
        assertThat(url, containsString("app_b"));
        assertThat(url, containsString("Environment"));
        assertThat(url, containsString("Ttt"));
        assertThat(url, containsString("Environment"));
        assertThat(url, containsString("Ppp"));
        assertThat(url, containsString("Latest%20deployment%20job%20for%20App%20Server%20and%20Env"));
    }

    @Test
    public void createDeploymentsWebUriWithDeprecatedConstructor() {
        // arrange
        String baseUri = "https://liima/AMW_rest/resources";
        LiimaConfiguration liimaConfiguration = new LiimaConfiguration(baseUri);

        // act
        String ngWebUri = liimaConfiguration.createDeploymentsWebUri("server1", "application1", "TTTT");

        // assert
        assertThat(ngWebUri, not(startsWith(baseUri)));
        assertThat(ngWebUri, containsString("AMW_angular"));
        assertThat(ngWebUri, containsString("server1"));
        assertThat(ngWebUri, containsString("application1"));
        assertThat(ngWebUri, containsString("Tttt"));
    }

    @Test
    public void createDeploymentsWebUriWithNewConstructor() {
        // arrange
        @NotNull String webUri = "https://liima/AMW_web";
        String ngBaseWebUri = "https://liima/AMW_angular";
        LiimaConfiguration liimaConfiguration = new LiimaConfiguration(BASE_URL, webUri, ngBaseWebUri);

        // act
        String ngWebUri = liimaConfiguration.createDeploymentsWebUri("server1", "application1", "TTTT");

        // assert
        assertThat(ngWebUri, startsWith(ngBaseWebUri));
        assertThat(ngWebUri, containsString("server1"));
        assertThat(ngWebUri, containsString("application1"));
        assertThat(ngWebUri, containsString("Tttt"));
    }

    @Test
    public void createDeploymentsWebUriWithTrackingId() {
        // arrange
        @NotNull String webUri = "https://liima/AMW_web";
        String ngBaseWebUri = "https://liima/AMW_angular";
        LiimaConfiguration liimaConfiguration = new LiimaConfiguration(BASE_URL, webUri, ngBaseWebUri);

        // act
        Filters filters = new Filters.Builder()
                .add(FilterName.TRACKING_ID, FilterComparator.EQ, "123456")
                .build();
        String ngWebUri = liimaConfiguration.createDeploymentsWebUri(filters);

        // assert
        assertThat(ngWebUri, startsWith(ngBaseWebUri));
        assertThat(ngWebUri, containsString("123456"));
        assertThat(ngWebUri, containsString("Tracking%20Id"));
    }

}
