package ch.mobi.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 - 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.junit.Test;

import java.util.List;

import static ch.mobi.liima.client.FilterComparator.EQ;
import static ch.mobi.liima.client.FilterName.APPLICATION;
import static ch.mobi.liima.client.FilterName.APPLICATION_SERVER;
import static ch.mobi.liima.client.FilterName.ENVIRONMENT;
import static ch.mobi.liima.client.FilterName.STATE;
import static ch.mobi.liima.client.dto.DeploymentState.READY_FOR_DEPLOYMENT;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class FiltersTest {

    @Test
    public void getFilterStringForLatestOnly() {
        // arrange
        Filters filters = new Filters.Builder()
                .add(APPLICATION_SERVER, EQ, "jap_mobitor")
                .add(FilterName.LATEST_ONLY)
                .build();

        // act
        String filterString = filters.getFilterString();

        // assert
        String expectedFilter = "[{\"name\":\"Application server\",\"comp\":\"eq\",\"val\":\"jap_mobitor\"},{\"name\":\"Latest deployment job for App Server and Env\",\"comp\":\"eq\",\"val\":\"\"}]";
        assertThat(filterString, is(equalTo(expectedFilter)));
    }

    @Test
    public void getFilterStringForDeploymentState() {
        // arrange
        Filters filters = new Filters.Builder()
                .add(APPLICATION_SERVER, EQ, "jap_mobitor")
                .add(STATE, EQ, READY_FOR_DEPLOYMENT.getDisplayName())
                .build();

        // act
        String filterString = filters.getFilterString();

        // assert
        String expectedFilter = "[{\"name\":\"Application server\",\"comp\":\"eq\",\"val\":\"jap_mobitor\"},{\"name\":\"State\",\"comp\":\"eq\",\"val\":\"ready_for_deploy\"}]";
        assertThat(filterString, is(equalTo(expectedFilter)));
    }

    @Test
    public void getFilterStringForSingleEnvironment() {
        // arrange
        Filters filters = new Filters.Builder()
                .add(ENVIRONMENT, EQ, "C")
                .build();

        // act
        String filterString = filters.getFilterString();

        // assert
        String expectedFilter = "[{\"name\":\"Environment\",\"comp\":\"eq\",\"val\":\"C\"}]";
        assertThat(filterString, is(equalTo(expectedFilter)));
    }

    @Test
    public void getFilterStringForMultipleEnvironments() {
        // arrange
        Filters filters = new Filters.Builder()
                .add(ENVIRONMENT, EQ, asList("C", "I"))
                .build();

        // act
        String filterString = filters.getFilterString();

        // assert
        String expectedFilter = "[{\"name\":\"Environment\",\"comp\":\"eq\",\"val\":\"C\"},{\"name\":\"Environment\",\"comp\":\"eq\",\"val\":\"I\"}]";
        assertThat(filterString, is(equalTo(expectedFilter)));
    }

    @Test
    public void getFilterStringWithNoFilterArguments() {
        // arrange
        Filters filters = new Filters.Builder()
                .build();

        // act
        String filterString = filters.getFilterString();

        // assert
        String expectedFilter = "[]";
        assertThat(filterString, is(equalTo(expectedFilter)));
    }

    @Test
    public void getFilterStringWithNullValuesLists() {
        // arrange
        Filters filters = new Filters.Builder()
                .add(APPLICATION, EQ, (List<String>)null)
                .build();

        // act
        String filterString = filters.getFilterString();

        // assert
        String expectedFilter = "[]";
        assertThat(filterString, is(equalTo(expectedFilter)));
    }

}
