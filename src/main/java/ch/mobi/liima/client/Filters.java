package ch.mobi.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 - 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static ch.mobi.liima.client.FilterComparator.EQ;

public class Filters {

    private static final Logger LOG = LoggerFactory.getLogger(Filters.class);

    private final List<Filter> filters;

    private Filters(List<Filter> filters) {
        this.filters = filters;
    }

    public String getFilterString() {
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.writeValueAsString(filters);

        } catch (JsonProcessingException e) {
            LOG.error("Could not serialize filters!", e);
        }

        return "";
    }

    public static class Builder {

        private List<Filter> filters = new ArrayList<>();

        public Filters build() {
            return new Filters(filters);
        }

        public Builder add(FilterName filterName, FilterComparator comparator, String value) {
            Filter f = new Filter(filterName, comparator, value);
            filters.add(f);

            return this;
        }

        public Builder add(FilterName filterName, FilterComparator comparator, List<String> values) {
            if (values != null && values.size() > 0) {
                values.forEach(value -> add(filterName, comparator, value));
            }

            return this;
        }



        public Builder add(FilterName filterName) {
            return add(filterName, EQ, "");
        }
    }

}
