package ch.mobi.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 - 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Filter {

    @JsonProperty("name") private FilterName name;
    @JsonProperty("comp") private FilterComparator comparator;
    @JsonProperty("val") private String value;

    @JsonCreator
    public Filter(FilterName name, FilterComparator comparator, String value) {
        this.name = name;
        this.comparator = comparator;
        this.value = value;
    }

    public FilterName getName() {
        return name;
    }

    public FilterComparator getComparator() {
        return comparator;
    }

    public String getValue() {
        return value;
    }
}
