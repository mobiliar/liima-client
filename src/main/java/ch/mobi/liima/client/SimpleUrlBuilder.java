package ch.mobi.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import static java.util.stream.Collectors.joining;

class SimpleUrlBuilder {

    private final List<String> pathElements;
    private final List<Pair<String, String>> queryParameter;

    private SimpleUrlBuilder() {
        this.pathElements = new ArrayList<>();
        this.queryParameter = new ArrayList<>();
    }

    public static String build(Consumer<SimpleUrlBuilder> builderValues) {
        SimpleUrlBuilder builder = new SimpleUrlBuilder();
        builderValues.accept(builder);
        return builder.build();
    }

    private String build() {
        String path = pathElements.stream().collect(joining("/"));
        String query = queryParameter.stream()
                                     .filter(p -> !Objects.isNull(p.getRight()))
                                     .filter(p -> !p.getRight().isEmpty())
                                     .map(p -> p.getLeft() + "=" + p.getRight())
                                     .collect(joining("&"));
        if (!query.isEmpty()) {
            query = "?" + query;
        }
        return path + query;
    }

    public SimpleUrlBuilder path(String path) {
        if (path != null && !path.isEmpty()) {
            if (path.charAt(0) == '/') {
                path = path.substring(1);
            }
            if (path.charAt(path.length() - 1) == '/') {
                path = path.substring(0, path.length() - 1);
            }
            pathElements.add(path);
        }
        return this;
    }

    public SimpleUrlBuilder queryParameter(String key, String value) {
        if (key != null && !key.isEmpty()) {
            queryParameter.add(Pair.of(key, value));
        }
        return this;
    }

    public SimpleUrlBuilder queryParameter(String key, List<String> values) {
        if (values != null) {
            values.forEach(value -> queryParameter(key, value));
        }
        return this;
    }

}
