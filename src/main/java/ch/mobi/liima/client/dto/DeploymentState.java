package ch.mobi.liima.client.dto;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

/**
 * Deployment state from liima
 * ch.puzzle.itc.mobiliar.business.deploy.entity.DeploymentEntity$DeploymentState
 */
public enum DeploymentState {
    success("success"),
    failed("failed"),
    canceled("canceled"),
    rejected("rejected"),
    READY_FOR_DEPLOYMENT("ready_for_deploy"),
    PRE_DEPLOYMENT("pre_deploy"),
    progress("progress"),
    simulating("simulating"),
    delayed("delayed"),
    scheduled("scheduled"),
    requested("requested");

    private String displayName;

    DeploymentState(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
